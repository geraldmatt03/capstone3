import React, {useState} from 'react';
import './App.css';
import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext';

// components
import AppNavbar from './components/AppNavbar';

// pages
import Home from './pages/Home';
import SignIn from './pages/SignIn';
import SignOut from './pages/SignOut';
import Register from './pages/Register';
import Contact from './pages/ContactPage';
import Cart from './pages/Carts';
import Products from './pages/Products';
import SpecificProduct from './pages/SpecificProduct';
import NotFound from './pages/NotFound';

// routes
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';


function App() {
  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/signIn" element={<SignIn/>}/>
            <Route path="/signOut" element={<SignOut/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/contact" element={<Contact/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/:productId" element={<SpecificProduct/>}/>
            <Route path="/cart" element={<Cart/>}/>
            <Route path="*" element={<NotFound/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    );
}

export default App;
