import React, {useState, useEffect} from 'react';
import {Table} from 'react-bootstrap';

// functionalities
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import DeleteProduct from './DeleteProduct';

export default function AdminView({productsData, fetchData}){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		const productsArr = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.category}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Available" : "Unavailable"}</td>
					<td><EditProduct product={product._id} fetchData={fetchData}/></td>
					<td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/></td>
					<td><DeleteProduct product={product._id} fetchData={fetchData}/></td>
				</tr>
				)
		})

		setProducts(productsArr);
	}, [productsData])

	return(
		<>
			<div className="text-center my-4">
				<h1>Admin Dashboard</h1>
				<AddProduct fetchData={fetchData}/>
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Product Id</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Category</th>
						<th>Availability</th>
						<th colSpan="3" className="text-center">Actions</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>
		</>
		);
}