import React from 'react';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product, isActive, fetchData}){
	const archiveToggle = (productId) => {
		fetch(`https://pacific-citadel-18583.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Nice!!",
					icon: "success",
					text: "Product Successfully Disabled"
				})

				fetchData();

			} else{
				Swal.fire({
					title: "Oooops!!",
					icon: "error",
					text: "Something's Wrong. Try Again"
				})

				fetchData();

			}
		})
	}

	const unarchiveToggle = (productId) => {
		fetch(`https://pacific-citadel-18583.herokuapp.com/products/${productId}/reactivate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Nice!!",
					icon: "success",
					text: "Product Successfully Enabled"
				})

				fetchData();

			} else{
				Swal.fire({
					title: "Oooops!!",
					icon: "error",
					text: "Something's Wrong. Try Again"
				})

				fetchData();
				
			}
		})
	}

	return(
		<>
			{
				isActive ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>
				:
				<Button variant="success" size="sm" onClick={() => unarchiveToggle(product)}>Unarchive</Button>
			}
		</>
		)
}