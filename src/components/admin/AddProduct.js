import React, {useState} from 'react';
import {Button, Form, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddProduct({fetchData}){
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');

	const [showAdd, setShowAdd] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const addProduct = (e) => {
		e.preventDefault();

		fetch('https://pacific-citadel-18583.herokuapp.com/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Nice!!!",
					icon: "success",
					text: `${name} Successfully Added`
				})

				closeAdd();
				fetchData();
			} else if(data.message === 'Error in Adding New Product. Please Complete All Details'){
				Swal.fire({
					title: "Oh Noooo!",
					icon: "error",
					text: "Please Fill In All Input Fields"
				})
				closeAdd();
				fetchData();
			} else {
				Swal.fire({
					title: "Something's Wrong",
					icon: "error",
					text: "Please Try Again"
				})
				closeAdd();
				fetchData();
			}

			setName('');
			setDescription('');
			setPrice('');
			setCategory('');
		})

	}

	return(
		<>
			<Button variant="success" onClick={openAdd}>Add New Product</Button>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Product Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/> 
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/> 
						</Form.Group>

						<Form.Group>
							<Form.Label>Price (in Php)</Form.Label>
							<Form.Control type="Number" value={price} onChange={e => setPrice(e.target.value)} required/> 
						</Form.Group>

						<Form.Group>
							<Form.Label>Category</Form.Label>
							<Form.Control type="text" value={category} onChange={e => setCategory(e.target.value)} required/> 
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Cancel</Button>
						<Button variant="success" type="submit">Add</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
		);
}