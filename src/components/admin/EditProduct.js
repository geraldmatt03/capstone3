import React, {useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProduct({product, fetchData}) {
	const [productId, setProductId] = useState('');

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');

	const [showEdit, setShowEdit] = useState(false);

	const openEdit = (productId) => {
		fetch(`https://pacific-citadel-18583.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setCategory(data.category);
		})

		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
		setCategory('');
	};

	const editProduct = (e) => {
		e.preventDefault();

		fetch(`https://pacific-citadel-18583.herokuapp.com/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.message === 'Product Updated Successfully'){
				Swal.fire({
					title: "Nice!!",
					icon: "success",
					text: "Product Updated Successfully"
				})
				console.log(data.message)

				closeEdit();
				fetchData();

			} else {
				Swal.fire({
					title: "Oh Noooo!",
					icon: "error",
					text: "Please Check Your Inputs And Try Again"
				})

				closeEdit();
				fetchData();
			}
		})
	};

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}>Edit</Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Product Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/> 
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/> 
						</Form.Group>

						<Form.Group>
							<Form.Label>Price (in Php)</Form.Label>
							<Form.Control type="Number" value={price} onChange={e => setPrice(e.target.value)} required/> 
						</Form.Group>

						<Form.Group>
							<Form.Label>Category</Form.Label>
							<Form.Control type="text" value={category} onChange={e => setCategory(e.target.value)} required/> 
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Cancel</Button>
						<Button variant="success" type="submit">Edit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
		);
}