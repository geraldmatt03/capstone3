import React, {useState} from 'react';
import {Button, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteProduct({product, fetchData}){
	const [showDelete, setShowDelete] = useState(false);

	const openDelete = () => setShowDelete(true);
	const closeDelete = () => setShowDelete(false);

	const deleteToggle = (productId) => {
		fetch(`https://pacific-citadel-18583.herokuapp.com/products/${product}/delete`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.message === 'Product Deleted Successfully'){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product Successfully Deleted"
				})

				closeDelete();
				fetchData();

			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Can't Delete File. Try Again."
				})

				closeDelete();
				fetchData();
			}
		})
	}

	return(
		<>
			<Button variant="warning" size="sm" onClick={openDelete}>Delete</Button>

			<Modal show={showDelete} onHide={closeDelete}>
				<Modal.Header closeButton>
					<Modal.Title>Delete Product</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<p>Are you sure you want to delete product?</p>
				</Modal.Body>

				<Modal.Footer>
					<Button variant="secondary" onClick={closeDelete}>Close</Button>
					<Button variant="success" onClick={() => deleteToggle()}>Delete</Button>
				</Modal.Footer>
			</Modal>
		</>
		);
}