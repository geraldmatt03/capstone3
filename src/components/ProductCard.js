import React from 'react';
import {Card, Button, Row, Col} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {
	const {_id, name, description, price, category} = productProp;

	return(
		<>
			<div>
				<Row>
				<Col className="p-3 mb-3">
					<Card className="my-3 cardProduct">
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Category</Card.Subtitle>
							<Card.Text>{category}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Button variant="primary" as={Link} to={`/products/${_id}`}>Check Details</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			</div>
		</>
		);
}

ProductCard.propTypes = {
	orderProps: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		category: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}