import React, {useState, useEffect} from 'react';
import {Button} from 'react-bootstrap';
import ProductCard from '../ProductCard';

export default function UserView({productsData, handleMouse, handleKeyboard, handleAccessories, handleAudio, handleRemove}){
	const [products, setProducts] = useState([]);

	useEffect(() => {
		const productsArr = productsData.map(product => {
			if(product.isActive === true){
				return(
					<ProductCard productProp={product} key={product._id}/>
					)
			} else{
				return null;
			}
		})

		setProducts(productsArr);
	}, [productsData]);

	return(
		<>
			<Button onClick={() => handleMouse("mouse")}>See All Mice</Button>{' '}
			<Button onClick={() => handleKeyboard("keyboard")}>See All Keyboards</Button>{' '}
			<Button onClick={() => handleAccessories("accessories")}>See All Accessories</Button>{' '}
			<Button onClick={() => handleAudio("audio")}>See All Audio</Button>{' '}
			{products}
		</>
		);
}