import React from 'react';
import {Row, Col, Card, Container} from 'react-bootstrap';


export default function Highlights(){
	return(
		<Container className="grid">
		<Row>
			<Col xs={12} md={6} lg={6} className="p-3">
				<Card className="cardHighlight p-3 mb-3">
					<Card.Img variant="top" src="https://vision-techno.com/assets/images/columnsblocks/product99/29751.png" className="cardImg"/>
					<Card.Body>
						<Card.Title>
							<h2>Gaming Mice</h2>
						</Card.Title>
						<Card.Text>
							Gaming Mice that we offer are best suited for gamers as it is built for gaming ergonomics, lightweight, equipped with more buttons built for customization.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
					
			<Col xs={12} md={6} lg={6} className="p-3">
				<Card className="cardHighlight p-3 mb-3">
					<Card.Img variant="top" src="https://assets2.razerzone.com/images/pnx.assets/897694fbce88b6f42744665a21de0230/razer-kraken-2019-usp-sound-mobile[1].jpg" className="cardImg"/>
					<Card.Body>
						<Card.Title>
							<h2>Gaming Audio</h2>
						</Card.Title>
						<Card.Text>
							Gaming Audio Equipments that we offer are best suited for gamers that wants to gain competitive advantage with their contemporaries. Gaming Headsets that has a good surround sound, capable of capturing enemy footsteps and gunfire from a distance. Gaming Microphones that is built to record crisp and clear sounds that is good for communicating and recording for streamers.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={6} lg={6} className="p-3">
				<Card className="cardHighlight p-3 mb-3">
					<Card.Img variant="top" src="https://www.notebookcheck.net/fileadmin/Notebooks/News/_nc3/20180111_Asus_ROG_Strix_Flare_Keyboard.JPG" className="cardImg"/>
					<Card.Body>
						<Card.Title>
							<h2>Gaming Keyboards</h2>
						</Card.Title>
						<Card.Text>
							Gaming Keyboards that we offer are best suited for gamers as it is built for someone with fast hands, strongly built for rigorous usage. It is also customizable, from hot swappables, gateron, and colored switches.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={6} lg={6} className="p-3">
				<Card className="cardHighlight p-3 mb-3">
					<Card.Img variant="top" src="https://www.corsair.com/corsairmedia/sys_master/productcontent/st100-rgb-headset-stand-Content-25.jpg" className="cardImg"/>
					<Card.Body>
						<Card.Title>
							<h2>Gaming Accessories</h2>
						</Card.Title>
						<Card.Text>
							We also offer a variety of gaming accessories. RGB lightings to equip to your battle stations that can be set up depending on the mood. The likes of headset stands to keep your headsets in one place tidying up your place, and many more.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		</Container>
		);
}