import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){
	return(
		<Row className="my-auto text-center">
			<Col className="p-5">
				<h1 className="mb-3"><strong>GM's Gaming Hub</strong></h1>
				<p className="my-3"><em>Your One Stop Shop of Gaming Peripherals</em></p>
				<Button varirant="primary" as={Link} to="/products">SHOP NOW!</Button>
			</Col>
		</Row>
		);
}