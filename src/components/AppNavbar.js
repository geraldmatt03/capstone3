import React, {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import '../App.css'

export default function AppNavbar(){
	const {user} = useContext(UserContext);

	return (
		<Navbar bg="dark" expand="lg" variant="dark" className="mb-5" id="#navibar">
			<Navbar.Brand as={Link} to="/" className="ms-2"> <img
          alt=""
          src="./favicon.ico"
          width="30"
          height="30"
          className="d-inline-block align-top"/>{''}
          Gaming Hub
          </Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse>
				<Nav className="ms-auto">
					<Nav.Link as={Link} to="/products">Products</Nav.Link>
					<Nav.Link as={Link} to="/contact">Contact</Nav.Link>

					{ (user.accessToken !== null) ? 
						<>
							<Nav.Link as={Link} to="/cart">Cart</Nav.Link>
							<Nav.Link as={Link} to="/signOut">Sign Out</Nav.Link>
						</>
						:

						<>
							<Nav.Link as={Link} to="/signIn">Sign In</Nav.Link>
						</>
					}

				</Nav>
			</Navbar.Collapse>
		</Navbar>
		);
}

