import React, {useState, useEffect, useContext} from 'react';
import {Container, Card, Button} from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function SpecificProduct() {
	const {productId} = useParams();
	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');
	const [quantity, setQuantity] = useState(1)

	const addQuantity = (quantity) => {
		setQuantity(currQuantity => currQuantity + 1);
	};

	const reduceQuantity = (quantity) => {
		setQuantity(currQuantity => currQuantity - 1);
	};

	useEffect(() => {
		fetch(`https://pacific-citadel-18583.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setCategory(data.category);
		})

		console.log(quantity);
	}, [quantity])

	const addToCart = (productId, quantity) => {
		fetch('https://pacific-citadel-18583.herokuapp.com/carts/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Added To Cart'
				})

				navigate('/products');
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	return(
		<Container>
			<Card>
				<Card.Header>
					<Card.Title>{name}</Card.Title>
				</Card.Header>

				<Card.Body>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php: {price}</Card.Text>
					<Card.Subtitle>Category:</Card.Subtitle>
					<Card.Text>{category}</Card.Text>
					<Card.Subtitle>Quantity</Card.Subtitle>
					<Card.Text>
						<Button variant="light" size="sm"  disabled={(quantity === 1) ? true : false} onClick={(reduceQuantity)}>-</Button>
						{quantity}
						<Button variant="light" size="sm" onClick={(addQuantity)}>+</Button>
					</Card.Text>
				</Card.Body>

				<Card.Footer>
					{user.accessToken !== null ? 
						<div className="d-grid gap-2">
							<Button variant="primary" onClick={() => addToCart(productId, quantity)}>Add To Cart</Button>
						</div>

						:

						<Link className="btn btn-warning d-grid gap-2" to='/signIn'>Sign In to Purchase</Link>
					}
				</Card.Footer>
			</Card>
		</Container>
		);
}