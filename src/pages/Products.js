import React, {useContext, useEffect, useState} from 'react';
import UserContext from '../UserContext';

// components
import UserView from '../components/user/UserView';
import AdminView from '../components/admin/AdminView';

export default function Products(){
	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	const handleMouse = (category) => {
		const mouseArr =  allProducts.filter(product => product.category === "mouse");
		setAllProducts(mouseArr);
	}

	const handleKeyboard = (category) => {
		const keyboardArr =  allProducts.filter(product => product.category === "keyboard");
		setAllProducts(keyboardArr);
	}

	const handleAccessories = (category) => {
		const accessoriesArr =  allProducts.filter(product => product.category === "accessories");
		setAllProducts(accessoriesArr);
	}

	const handleAudio = (category) => {
		const audioArr =  allProducts.filter(product => product.category === "audio");
		setAllProducts(audioArr);
	}

	
	const fetchData = () => {
		fetch('https://pacific-citadel-18583.herokuapp.com/products/allProducts')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data);
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return(
		(user.isAdmin === true) ? 
			<AdminView productsData={allProducts} fetchData={fetchData} />
			:
			<UserView productsData={allProducts} handleMouse={handleMouse} handleKeyboard={handleKeyboard} handleAccessories={handleAccessories} handleAudio={handleAudio}/>
		);
}