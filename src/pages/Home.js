import React from 'react';

// components
import Banner from '../components/home/Banner';
import Highlights from '../components/home/Highlights';

export default function Home() {
	return(
			<>
				<Banner />
				<Highlights />
			</>
		)
};