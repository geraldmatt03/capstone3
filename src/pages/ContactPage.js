import React from 'react';
import {Form, Button, Row, Col} from 'react-bootstrap';


export default function Contact(){
	return(
		<>
		<Row className="text-center d-flex flex-row justify-content-center">
			<h1 className="text-center">Contact Us</h1>
			<Col lg={6} sm={12} md={8} className="border border-muted text-center">
				<Form>
					<Form.Group>
						<Form.Label>Email Address</Form.Label>
						<Form.Control 
							type="email"
							placeholder="user@email.com"
							required
							/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control 
							type="text"
							placeholder="09000000000"
							required
							/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Message/Concern:</Form.Label>
						<Form.Control 
							as="textarea"
							placeholder="Insert Message Here"
							rows={5}
							columns={5}
							required
							/>
					</Form.Group>
					
					<Button variant="primary" type="submit" className="mt-3">Send</Button>
				</Form>
			</Col>
		</Row>
		</>
		)
}