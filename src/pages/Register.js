import React, {useState, useEffect, useContext} from 'react';
import {Row, Col, Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [completeAddress, setCompleteAddress] = useState('');

	const [isActive, setIsActive] = useState(true);

	const navigate = useNavigate();

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== '' && completeAddress !== '') && (password1 === password2)){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2, mobileNo, completeAddress]);

	const register = (e) => {
		e.preventDefault();

		fetch('https://pacific-citadel-18583.herokuapp.com/users/register', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo,
				completeAddress: completeAddress
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.message === 'This email already exists. Pick another to continue'){
				Swal.fire({
					title: "Oooops!",
					icon:  "error",
					text: "Email Address Taken. Please Pick Another To Continue"
				})
			} else{
				Swal.fire({
				title: "Yaaaaaayy!",
				icon: "success",
				text: "You have successfully registered. Please Login to Continue Transaction."
				})

				setFirstName('');
				setLastName('');
				setEmail('');
				setPassword1('');
				setPassword2('');
				setMobileNo('');
				setCompleteAddress('');

				navigate('/signIn');
			}
		})
	}

	return(
		(user.accessToken !== null) ?

		<Navigate to="/login" />

		:
		<>
		<Row className="text-center d-flex flex-row justify-content-center">
			<h1 className="text-center">Register</h1>
			<Col lg={6} sm={12} md={8} className="border border-muted text-center">
				<Form onSubmit={(e) => register(e)}>
					<Form.Group className="p-2">
						<Form.Label>First Name</Form.Label>
						<Form.Control 
							type="text"
							placeholder="ex. Juan"
							required
							value={firstName}
							onChange={e => setFirstName(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="p-2">
						<Form.Label>Last Name</Form.Label>
						<Form.Control 
							type="text"
							placeholder="ex. Dela Cruz"
							required
							value={lastName}
							onChange={e => setLastName(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="p-2">
						<Form.Label>Email Address</Form.Label>
						<Form.Control 
							type="email"
							placeholder="Enter Email"
							required
							value={email}
							onChange={e => setEmail(e.target.value)}
							/>
						<Form.Text className="text-muted">We'll never share your Email with anyone else</Form.Text>
					</Form.Group>

					<Form.Group className="p-2">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Enter Password"
							required
							value={password1}
							onChange={e => setPassword1(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="p-2">
						<Form.Label>Verify Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Verify Password"
							required
							value={password2}
							onChange={e => setPassword2(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="p-2">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Mobile Number"
							requiredvalue={mobileNo}
							onChange={e => setMobileNo(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="p-2">
						<Form.Label>Complete Address</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Complete Address"
							required
							value={completeAddress}
							onChange={e => setCompleteAddress(e.target.value)}
							/>
					</Form.Group>

					{isActive ?
						<Button variant="primary" type="submit" className="mt-3">Register</Button>
						:
						<Button variant="dark" type="submit" disabled className="mt-3">Register</Button>
					}
					

				</Form>
			</Col>
		</Row>

		<div className="text-center mt-1">
			<p className="text-center">Already Have an Account?</p>
				<Link className="text-center" to="/signIn">Click Here</Link>
		</div>
		</>
		);
}