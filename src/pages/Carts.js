import React, {useState, useEffect, useContext} from 'react';
import {Container, Card, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

import NotFound from './NotFound';


export default function Cart() {
	const {user} = useContext(UserContext);

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState(1);
	const [totalAmount, setTotalAmount] = useState(0);


	useEffect(() => {
		fetch('https://pacific-citadel-18583.herokuapp.com/carts/cart', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.products.name);
			setPrice(data.products.price);
			setQuantity(data.products.quantity);
			setTotalAmount(data.totalAmount);
		})

		console.log(quantity);
	}, [name, price, quantity, totalAmount])

	

	return(
		(user.isAdmin === true) ?
		<NotFound/>

		:

		<Container>
			<Card>
				<Card.Header>
					<Card.Title>Cart</Card.Title>
				</Card.Header>

				<Card.Body>
					<Card.Subtitle>Product Name:</Card.Subtitle>
					<Card.Text>{name}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php: {price}</Card.Text>
					<Card.Subtitle>Quantity</Card.Subtitle>
					<Card.Text>
						<Button variant="light" size="sm">-</Button>
						{quantity}
						<Button variant="light" size="sm">+</Button>
					</Card.Text>
					<Card.Subtitle>Subtotal:</Card.Subtitle>
					<Card.Text>{price}*{quantity}</Card.Text>
				</Card.Body>

				<Card.Footer>
					
				</Card.Footer>
			</Card>
		</Container>
		);
}