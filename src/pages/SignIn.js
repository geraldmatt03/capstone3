import React, {useState, useEffect, useContext} from 'react';
import {Row, Col, Form, Button, Card} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext'

export default function SignIn(){
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(true);
	const navigate = useNavigate();

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password]);

	const signIn = (e) => {
		e.preventDefault();

		fetch('https://pacific-citadel-18583.herokuapp.com/users/login', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken});
				Swal.fire({
					title: "Success",
					icon: "success",
					text: `Welcome ${email}`
				});

				fetch('https://pacific-citadel-18583.herokuapp.com/users/getUserDetails', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)

					if(result.isAdmin === true){
						localStorage.setItem('email', result.email);
						localStorage.setItem('isAdmin', result.isAdmin);
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						navigate('/products');
					} else {
						navigate('/');
					}
				})

			} else if(data.message === 'Incorrect Email Address'){
				Swal.fire({
					title: "Oooops!",
					icon: "error",
					text: "Wrong Email Address"
				})
			} else {
				Swal.fire({
					title: "Oooops!",
					icon: "error",
					text: "Something is Wrong. Check Credentials."
				})
			}

			setEmail('');
			setPassword('');
		})
	}

	return(
		(user.accessToken !== null) ?

		<Navigate to="/products"/>

		:
			
		<Row>
			<Col lg={6} md={12} className="mb-3">
				<Card className="cardSignIn">
					<Card.Body>
						<Card.Title className="mb-3">Sign In</Card.Title>
						<Form onSubmit={(e) => signIn(e)}>
							<Form.Group>
								<Form.Label>Email Address</Form.Label>
								<Form.Control
									size="sm" 
									type="email"
									placeholder="Enter Email"
									required
									value={email}
									onChange={e => setEmail(e.target.value)}
								/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Password</Form.Label>
								<Form.Control
									size="sm"  
									type="password"
									placeholder="Enter Password"
									required
									value={password}
									onChange={e => setPassword(e.target.value)}
								/>
							</Form.Group>
							{isActive ?
							<Button variant="primary" type="submit" className="mt-3">Sign In</Button>
							:
							<Button variant="dark" type="submit" className="mt-3" disabled>Sign In</Button>
							}
						</Form>
					</Card.Body>
				</Card>
			</Col>

			<Col lg={6} md={12} className="mb-3">
				<Card className="cardSignIn">
					<Card.Body>
						<Card.Title className="mb-3">Don't Have An Account Yet? Register</Card.Title>
						<Card.Subtitle className="mb-2">Why Register?</Card.Subtitle>
						<Card.Text as="div">
							<ul>
								<li>Easier To Check Out Products</li>
								<li>Updates On What's New and What's Available</li>
								<li>Ease Of Access</li>
							</ul>
						</Card.Text>
						<Button variant="primary" className="mt-3" as={Link} to="/register">Register</Button>
					</Card.Body>
				</Card>				
			</Col>
		</Row>
		);
}